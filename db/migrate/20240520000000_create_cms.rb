class CreateCms < ActiveRecord::Migration[7.1]
  def change
    create_table "cms_sites", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.string  "domain"
      t.string  "locale"
      t.integer "version"
      t.text    "properties"
      t.boolean "is_deleted"
      t.index   "domain"
    end

    create_table "cms_trees", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.integer "site_id"
      t.integer "page_id"
      t.integer "parent_id"
      t.integer "depth"
      t.timestamps
      t.index ["page_id", "parent_id"], name: "depth_index"
    end

    create_table "cms_paths", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.integer "site_id"
      t.integer "page_id"
      t.string  "path"
      t.string  "locale"
      t.timestamps
      t.index ["path"], name: "path_index"
    end

    create_table "cms_pages", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.integer   "site_id"
      t.integer   "parent_id"
      t.integer   "version"
      t.string    "name"
      t.string    "type"
      t.text      "properties"
      t.integer   "position"
      t.boolean   "is_published"
      t.boolean   "is_deleted"
      t.timestamps
    end

    create_table "cms_contents", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.integer "site_id"
      t.integer "page_id"
      t.boolean "is_published"
      t.integer "version"
      t.integer "author_id"
      t.string  "locale"
      t.integer "format"
      t.string  "title"
      t.string  "short_title"
      t.string  "path_segment"
      t.text    "content"
      t.boolean "is_deleted"
      t.timestamps
      t.index ["page_id", "is_published"]
    end

    create_table "cms_assets", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.integer "site_id"
      t.boolean "is_published"
      t.integer "position"
      t.integer "version"
      t.boolean "is_deleted"
      t.timestamps
    end

    create_table "cms_labels", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.integer "asset_id"
      t.string  "locale"
      t.string  "label"
      t.text    "description"
      t.timestamps
    end

    create_table "cms_links", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.integer "src_page_id"
      t.integer "dst_page_id"
      t.string  "label"
      t.timestamps
    end
  end
end