Gem::Specification.new do |spec|
  spec.name        = "cms_module"
  spec.version     = "0.1.0"
  spec.authors     = ["Nest Developers"]
  spec.summary     = "CMS for Nest"
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir["{app,config,db,lib}/**/*"]
  end
  spec.add_dependency "rails"
  #spec.add_dependency "actionpack-page_caching"
  #spec.add_dependency "rails-observers"
  #spec.add_development_dependency "minitest"
end
