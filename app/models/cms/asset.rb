class Cms::Assets < ActiveRecord::Base
  self.table_name = 'cms_assets'

  belongs_to :site
end
