class Cms::Content < ActiveRecord::Base
  self.table_name = 'cms_contents'

  belongs_to :site
  belongs_to :page
end