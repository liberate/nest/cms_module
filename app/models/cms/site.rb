class Cms::Site < ActiveRecord::Base
  self.table_name = 'cms_sites'

  has_many :pages,    dependent: :destroy
  has_many :assets,   dependent: :destroy
  has_many :paths,    dependent: :destroy
  has_many :contents, dependent: :destroy

  store :properties, coder: JSON
end