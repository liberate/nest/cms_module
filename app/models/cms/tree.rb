#
# A tree record records three things:
# page_id
# parent_id
# depth
#
# Imagine a simple tree:
#
#   A
#   '+- B
#    |
#    `- C
#       |
#       D
#
# This would result in Tree rows like so:
#
# parent_id   page_id   depth
# A           B           1
# A           C           1
# A           D           2
# C           D           1
#

class Cms::Tree < ActiveRecord::Base
  self.table_name = 'cms_trees'

  def self.update(page)
    depth, valid_ids = recursive_update(page, page.parent)
    Cms::Tree.delete_all("page_id = ? AND id NOT IN [?]", page.id, valid_ids)
  end

  private

  def self.recusive_update(page, parent)
    if parent.nil?
      depth = 0
      valid_ids = []
    else
      depth, valid_ids = recusive_update(parent, parent.parent)
      depth += 1
      valid_ids << Cms::Tree.find_or_create_by!(page_id: page.id, parent_id: parent.id, depth: depth).id
    end
    return [depth, valid_ids]
  end

end
