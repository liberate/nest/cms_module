class Cms::Page < ActiveRecord::Base
  self.table_name = 'cms_pages'

  belongs_to :site, optional: true
  belongs_to :parent, class_name: 'Page', optional: true
  has_many :children, class_name: 'Page', foreign_key: 'parent_id', dependent: :destroy

  has_many :paths,       dependent: :destroy
  has_many :contents,    dependent: :destroy
  has_many :connections, dependent: :destroy
  has_many :links,       dependent: :destroy
  has_many :trees,       dependent: :destroy

  def add_page(attrs={})
    raise ArumentError unless persisted?
    child = Cms::Page.create!(attrs.merge(parent_id: self.id))
    #Cms::Tree.update(child)
    Cms::Path.update(child)
    return child
  end
end