# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.1].define(version: 2024_05_20_000000) do
  create_table "cms_assets", force: :cascade do |t|
    t.integer "site_id"
    t.boolean "is_published"
    t.integer "position"
    t.integer "version"
    t.boolean "is_deleted"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cms_contents", force: :cascade do |t|
    t.integer "site_id"
    t.integer "page_id"
    t.boolean "is_published"
    t.integer "version"
    t.integer "author_id"
    t.string "locale"
    t.integer "format"
    t.string "title"
    t.string "short_title"
    t.string "path_segment"
    t.text "content"
    t.boolean "is_deleted"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["page_id", "is_published"], name: "index_cms_contents_on_page_id_and_is_published"
  end

  create_table "cms_labels", force: :cascade do |t|
    t.integer "asset_id"
    t.string "locale"
    t.string "label"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cms_links", force: :cascade do |t|
    t.integer "src_page_id"
    t.integer "dst_page_id"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cms_pages", force: :cascade do |t|
    t.integer "site_id"
    t.integer "parent_id"
    t.integer "version"
    t.string "name"
    t.string "type"
    t.text "properties"
    t.integer "position"
    t.boolean "is_published"
    t.boolean "is_deleted"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cms_paths", force: :cascade do |t|
    t.integer "site_id"
    t.integer "page_id"
    t.string "path"
    t.string "locale"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["path"], name: "path_index"
  end

  create_table "cms_sites", force: :cascade do |t|
    t.string "domain"
    t.string "locale"
    t.integer "version"
    t.text "properties"
    t.boolean "is_deleted"
    t.index ["domain"], name: "index_cms_sites_on_domain"
  end

  create_table "cms_trees", force: :cascade do |t|
    t.integer "site_id"
    t.integer "page_id"
    t.integer "parent_id"
    t.integer "depth"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["page_id", "parent_id"], name: "depth_index"
  end

end
