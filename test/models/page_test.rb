require_relative '../test_helper'

class PageTest < ActiveSupport::TestCase
  def test_page_tree
    root = nil
    assert_difference 'Cms::Page.count', 4 do
      root = Cms::Page.create!
      root.add_page.add_page.add_page
    end
  end
end