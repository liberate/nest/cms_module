module CmsModule
  class Engine < ::Rails::Engine
    isolate_namespace Cms

    #
    # make db:migrate automatically pull in engine migrations:
    #
    initializer :append_migrations do |app|
      unless app.config.root.to_s =~ /test\/dummy/
        app.config.paths["db/migrate"] << config.paths["db/migrate"].first
      end
    end
  end
end